package domainapp.modules.hello.dom.hwo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

import org.apache.isis.applib.services.repository.RepositoryService;
import org.apache.isis.persistence.jdo.applib.services.IsisJdoSupport_v3_2;

@ExtendWith(MockitoExtension.class)
class Listings_Test {

    @Mock RepositoryService mockRepositoryService;
    @Mock IsisJdoSupport_v3_2 mockIsisJdoSupport_v3_2;

    Listings objects;

    @BeforeEach
    public void setUp() {
        objects = new Listings(mockRepositoryService, mockIsisJdoSupport_v3_2);
    }

    @Nested
    class create {

        @Test
        void happyCase() {

            // given
            final String someAddress = "Foobar";

            // expect
            when(mockRepositoryService.persist(
                    argThat((ArgumentMatcher<Listing>) simpleObject -> Objects.equals(simpleObject.getAddress(), someAddress)))
            ).then((Answer<Listing>) invocation -> invocation.getArgument(0));

            // when
            final Listing obj = objects.create(someAddress, Listing.Status.VACANT);

            // then
            assertThat(obj).isNotNull();
            assertThat(obj.getAddress()).isEqualTo(someAddress);
        }
    }

    @Nested
    class ListAll {

        @Test
        void happyCase() {

            // given
            final List<Listing> all = new ArrayList<>();

            // expecting
            when(mockRepositoryService.allInstances(Listing.class))
                    .thenReturn(all);

            // when
            final List<Listing> list = objects.listAll();

            // then
            assertThat(list).isEqualTo(all);
        }
    }
}
