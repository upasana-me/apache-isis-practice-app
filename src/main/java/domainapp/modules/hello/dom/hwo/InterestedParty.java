package domainapp.modules.hello.dom.hwo;

import com.google.common.collect.ComparisonChain;
import lombok.Getter;
import lombok.Setter;
import org.apache.isis.applib.annotation.*;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.VersionStrategy;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

@javax.jdo.annotations.PersistenceCapable(identityType = IdentityType.DATASTORE, schema = "real_estate" )
@javax.jdo.annotations.DatastoreIdentity(strategy = IdGeneratorStrategy.IDENTITY, column = "id")
@javax.jdo.annotations.Version(strategy= VersionStrategy.DATE_TIME, column ="version")
@DomainObject(auditing = Auditing.ENABLED)
@DomainObjectLayout()  // causes UI events to be triggered
public class InterestedParty implements Comparable<InterestedParty>{
    @Getter @Setter
    private float price;

    @Getter @Setter
    private Person person;

    public InterestedParty(final Person person, final float price) {
        this.person = person;
        this.price = price;
    }

    public String title() {
        return this.getPerson().getFirstName() + " " + this.getPerson().getLastName();
    }

    @Override
    public int compareTo(final InterestedParty other) {
        return ComparisonChain.start()
                .compare(this.getPerson().getLastName(), other.getPerson().getLastName())
                .compare(this.getPerson().getFirstName(), other.getPerson().getFirstName())
                .result();
    }
}