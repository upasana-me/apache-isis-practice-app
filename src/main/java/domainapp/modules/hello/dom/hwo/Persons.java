package domainapp.modules.hello.dom.hwo;

import java.util.List;

import javax.jdo.JDOQLTypedQuery;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.PromptStyle;
import org.apache.isis.applib.annotation.RestrictTo;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.services.repository.RepositoryService;
import org.apache.isis.persistence.jdo.applib.services.IsisJdoSupport_v3_2;

import domainapp.modules.hello.types.Name;

@DomainService(
        nature = NatureOfService.VIEW,
        objectType = "hello.Persons"
)
public class Persons {

    private final RepositoryService repositoryService;
    private final IsisJdoSupport_v3_2 isisJdoSupport;

    public Persons(
            final RepositoryService repositoryService,
            final IsisJdoSupport_v3_2 isisJdoSupport) {
        this.repositoryService = repositoryService;
        this.isisJdoSupport = isisJdoSupport;
    }

    @Action(semantics = SemanticsOf.NON_IDEMPOTENT)
    @ActionLayout(promptStyle = PromptStyle.DIALOG_MODAL)
    public Person create(
            final String firstName, final String lastName, final String email) {
        return repositoryService.persist(new Person(firstName, lastName, email));
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(promptStyle = PromptStyle.DIALOG_SIDEBAR)
    public List<Person> findByEmail(
            final String email) {
        JDOQLTypedQuery<Person> q = isisJdoSupport.newTypesafeQuery(Person.class);
        final QPerson cand = QPerson.candidate();
        q = q.filter(
                cand.email.indexOf(q.stringParameter("email")).ne(-1)
        );
        return q.setParameter("email", email)
                .executeList();
    }

    @Action(semantics = SemanticsOf.SAFE, restrictTo = RestrictTo.PROTOTYPING)
    public List<Person> listAll() {
        return repositoryService.allInstances(Person.class);
    }
}
