package domainapp.modules.hello.dom.hwo;

import java.util.*;

import javax.inject.Inject;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.VersionStrategy;

import com.google.common.collect.ComparisonChain;
import lombok.Getter;
import lombok.Setter;
import org.apache.isis.applib.annotation.*;
import org.apache.isis.applib.annotation.Collection;
import org.apache.isis.applib.services.message.MessageService;
import org.apache.isis.applib.services.repository.RepositoryService;
import org.apache.isis.applib.services.title.TitleService;

import domainapp.modules.hello.types.Name;

@javax.jdo.annotations.PersistenceCapable(identityType = IdentityType.DATASTORE, schema = "real_estate" )
@javax.jdo.annotations.DatastoreIdentity(strategy = IdGeneratorStrategy.IDENTITY, column = "id")
@javax.jdo.annotations.Version(strategy= VersionStrategy.DATE_TIME, column ="version")
@javax.jdo.annotations.Unique(name="Person_email_UNQ", members = {"email"})
@DomainObject(auditing = Auditing.ENABLED)
@DomainObjectLayout()  // causes UI events to be triggered
public class Person implements Comparable<Person> {

    private Person(){}

    public Person(final String firstName, final String lastName, final String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String title() {
        return getFirstName() + " " + getLastName();
    }

    @MemberOrder(name = "details", sequence = "1")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private String firstName;

    @MemberOrder(name = "details", sequence = "1")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private String lastName;

    @MemberOrder(name = "identity", sequence = "1")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private String email;
    public String validateEmail(String email) {
        return email.contains("@") ? null : "Email address must contain a '@'";
    }

    @Override
    public int compareTo(final Person other) {
        return ComparisonChain.start()
                .compare(this.getLastName(), other.getLastName())
                .compare(this.getFirstName(), other.getFirstName())
                .compare(this.getEmail(), other.getEmail())
                .result();
    }

    @MemberOrder(name = "details", sequence = "2")
    @Property(editing = Editing.DISABLED)
    private ArrayList<Listing> ownedListings = new ArrayList<Listing>();
    public List<Listing> getOwnedListings() {
        List<Listing> allListings = listings.listAll();
        Iterator<Listing> iterator = allListings.iterator();
        while (iterator.hasNext()) {
            Listing listing = iterator.next();
            SortedSet<Person> owners = listing.getOwners();
            Iterator<Person> personIter = owners.iterator();
            while (personIter.hasNext()) {
                Person person = personIter.next();
                if ( person.compareTo(this) == 0 && !ownedListings.contains(listing)) {
                    ownedListings.add(listing);
                    break;
                }
            }
        }
        return ownedListings;
    }

    @MemberOrder(name = "details", sequence = "2")
    @Property(editing = Editing.DISABLED)
    private ArrayList<Listing> rentedListings = new ArrayList<Listing>();
    public List<Listing> getRentedListings() {
        List<Listing> allListings = listings.listAll();
        Iterator<Listing> iterator = allListings.iterator();
        ArrayList<Listing> rentedListings = new ArrayList<Listing>();
        while (iterator.hasNext()) {
            Listing listing = iterator.next();
            SortedSet<Person> tenants = listing.getTenants();
            Iterator<Person> personIter = tenants.iterator();
            while (personIter.hasNext()) {
                Person person = personIter.next();
                if ( person.compareTo(this) == 0 && !rentedListings.contains(listing)) {
                    rentedListings.add(listing);
                    break;
                }
            }
        }
        return rentedListings;
    }

    @MemberOrder(name = "details", sequence = "2")
    @Property(editing = Editing.DISABLED)
    private ArrayList<Listing> interestedListings = new ArrayList<Listing>();
    public List<Listing> getInterestedListings() {
        List<Listing> allListings = listings.listAll();
        Iterator<Listing> iterator = allListings.iterator();
        ArrayList<Listing> interestedListings = new ArrayList<Listing>();
        while (iterator.hasNext()) {
            Listing listing = iterator.next();
            SortedSet<InterestedParty> interestedParties = listing.getInterestedParties();
            Iterator<InterestedParty> iter = interestedParties.iterator();
            while (iter.hasNext()) {
                InterestedParty interestedParty = iter.next();
                if ( interestedParty.getPerson().compareTo(this) == 0 && !interestedListings.contains(listing)) {
                    interestedListings.add(listing);
                    break;
                }
            }
        }
        return interestedListings;
    }

    @Inject RepositoryService repositoryService;
    @Inject TitleService titleService;
    @Inject MessageService messageService;

    @Inject Listings listings;
}