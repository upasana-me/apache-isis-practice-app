package domainapp.modules.hello.dom.hwo;

import java.util.List;

import javax.jdo.JDOQLTypedQuery;

import org.apache.isis.applib.annotation.Action;
import org.apache.isis.applib.annotation.ActionLayout;
import org.apache.isis.applib.annotation.DomainService;
import org.apache.isis.applib.annotation.NatureOfService;
import org.apache.isis.applib.annotation.PromptStyle;
import org.apache.isis.applib.annotation.RestrictTo;
import org.apache.isis.applib.annotation.SemanticsOf;
import org.apache.isis.applib.services.repository.RepositoryService;
import org.apache.isis.persistence.jdo.applib.services.IsisJdoSupport_v3_2;

import domainapp.modules.hello.types.Name;

@DomainService(
        nature = NatureOfService.VIEW,
        objectType = "hello.Listings"
)
public class Listings {

    private final RepositoryService repositoryService;
    private final IsisJdoSupport_v3_2 isisJdoSupport;

    public Listings(
            final RepositoryService repositoryService,
            final IsisJdoSupport_v3_2 isisJdoSupport) {
        this.repositoryService = repositoryService;
        this.isisJdoSupport = isisJdoSupport;
    }

    @Action(semantics = SemanticsOf.NON_IDEMPOTENT)
    @ActionLayout(promptStyle = PromptStyle.DIALOG_MODAL)
    public Listing create(
            @Name final String address, final Listing.Status status) {
        return repositoryService.persist(new Listing(address, status));
    }

    @Action(semantics = SemanticsOf.SAFE)
    @ActionLayout(promptStyle = PromptStyle.DIALOG_SIDEBAR)
    public List<Listing> findByAddress(
            @Name final String address) {
        JDOQLTypedQuery<Listing> q = isisJdoSupport.newTypesafeQuery(Listing.class);
        final QListing cand = QListing.candidate();
        q = q.filter(
                cand.address.indexOf(q.stringParameter("address")).ne(-1)
        );
        return q.setParameter("address", address)
                .executeList();
    }

    @Action(semantics = SemanticsOf.SAFE, restrictTo = RestrictTo.PROTOTYPING)
    public List<Listing> listAll() {
        return repositoryService.allInstances(Listing.class);
    }
}
