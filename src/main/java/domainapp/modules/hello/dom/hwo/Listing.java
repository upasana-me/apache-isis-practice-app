package domainapp.modules.hello.dom.hwo;

import java.util.*;

import javax.inject.Inject;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.VersionStrategy;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.apache.isis.applib.annotation.*;
import org.apache.isis.applib.annotation.Collection;
import org.apache.isis.applib.services.message.MessageService;
import org.apache.isis.applib.services.repository.RepositoryService;
import org.apache.isis.applib.services.title.TitleService;
import org.joda.time.LocalDate;

import domainapp.modules.hello.types.Name;

@javax.jdo.annotations.PersistenceCapable(identityType = IdentityType.DATASTORE, schema = "real_estate" )
@javax.jdo.annotations.DatastoreIdentity(strategy = IdGeneratorStrategy.IDENTITY, column = "id")
@javax.jdo.annotations.Version(strategy= VersionStrategy.DATE_TIME, column ="version")
@javax.jdo.annotations.Unique(name="Listing_address_UNQ", members = {"address"})
@DomainObject(auditing = Auditing.ENABLED)
@DomainObjectLayout()  // causes UI events to be triggered
public class Listing implements Comparable<Listing> {

    private Listing(){}

    enum Status {
        VACANT,
        IN_OFFER,
        OCCUPIED
    };

    enum Currency {
        EUR,
        INR,
        USD
    };

    public Listing(final String address, final Status status) {
        this.address = address;
        this.status = status;
    }

    public String title() {
        return "Listing: " + getAddress();
    }

    @MemberOrder(name = "identity", sequence = "1")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private String address;

    @MemberOrder(name = "details", sequence = "1")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private Status status;

    @MemberOrder(name = "details", sequence = "2")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private float rent;

    @MemberOrder(name = "details", sequence = "2")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private Currency currency;

    @MemberOrder(name = "details", sequence = "2")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private LocalDate startDate;
    public String validateStartDate(LocalDate date) {
        if ( this.endDate != null ) {
            if ( endDate.compareTo(date) > 0 ) {
                return null;
            } else {
                return "Start date should be before the end date";
            }
        } else {
            LocalDate today = new LocalDate();
            System.out.println("today : " + today);
            if (date.compareTo(today) < 0 ) {
                System.out.println("is in the past");
                return "Start date should be in the future";
            } else {
                System.out.println("is not in the past");
                return null;
            }
        }
    }

    @MemberOrder(name = "details", sequence = "2")
    @Getter @Setter
    @Property(editing = Editing.ENABLED)
    private LocalDate endDate;
    public String validateEndDate(LocalDate date) {
        if ( this.startDate != null ) {
            if ( startDate.compareTo(date) < 0 ) {
                return null;
            } else {
                return "End date should be after the start date";
            }
        } else {
            LocalDate today = new LocalDate();
            if ( date.compareTo(today) < 0) {
                return "End date should be in the future";
            } else {
                return null;
            }
        }
    }

    @MemberOrder(name = "details", sequence = "2")
    @Property(editing = Editing.DISABLED)
    private int interestedPartiesCount;
    public int getInterestedPartiesCount() { return interestedParties.size(); }

    @MemberOrder(name = "details", sequence = "2")
    @Property(editing = Editing.DISABLED)
    private float highestBid;
    public float getHighestBid() {
        float highest = 0;
        Iterator<InterestedParty> it = interestedParties.iterator();
        while (it.hasNext()) {
            // Get element
            InterestedParty interestedParty = it.next();
            if ( highest < interestedParty.getPrice() ) {
                highest = interestedParty.getPrice();
            }
        }
        return highest;
    }

    @MemberOrder(name = "details", sequence = "2")
    @Property(editing = Editing.DISABLED)
    private Person highestBidder;
    public Person getHighestBidder() {
        Person highestBidder = null;
        float highest = 0;
        Iterator<InterestedParty> it = interestedParties.iterator();
        while (it.hasNext()) {
            // Get element
            InterestedParty interestedParty = it.next();
            if ( highest < interestedParty.getPrice() ) {
                highest = interestedParty.getPrice();
                highestBidder = interestedParty.getPerson();
            }
        }
        return highestBidder;
    }

    @Action(semantics = SemanticsOf.NON_IDEMPOTENT_ARE_YOU_SURE, associateWith = "address")
    @ActionLayout(position = ActionLayout.Position.PANEL)
    public void delete() {
        final String title = titleService.titleOf(this);
        messageService.informUser(String.format("'%s' deleted", title));
        repositoryService.removeAndFlush(this);
    }

    @Action(
            semantics = SemanticsOf.IDEMPOTENT,
            associateWith = "owners"
    )
    public Listing addOwner(Person person) {
        if ( !this.owners.contains(person) ) {
            this.owners.add(person);
            repositoryService.persist(person);
        }
        return this;
    }

    public List<Person> choices0AddOwner() {
        List<Person> allPersons = persons.listAll();
        List<Person> mayBeOwners = new ArrayList<>();
        Iterator<Person> iterator = allPersons.iterator();

        List<Person> interestedPersons = new ArrayList<Person>();
        Iterator<InterestedParty> interestedPartyIterator = this.interestedParties.iterator();

        while (interestedPartyIterator.hasNext()) {
            InterestedParty interestedParty = interestedPartyIterator.next();
            interestedPersons.add(interestedParty.getPerson());
        }
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if ( !this.tenants.contains(person) && !interestedPersons.contains(person) ) {
                mayBeOwners.add(person);
            }
        }
        return mayBeOwners;
    }

    @Action(
            semantics = SemanticsOf.IDEMPOTENT,
            associateWith = "owners", associateWithSequence = "2"
    )
    public Listing removeOwner(Person person) {
        this.owners.remove(person);
        return this;
    }

    @Collection()
    @Getter @Setter
    private SortedSet<Person> owners = new TreeSet<Person>();

    @Action(
            semantics = SemanticsOf.IDEMPOTENT,
            associateWith = "interestedParties"
    )
    public Listing addInterestedParty(final Person person, final float price) {
        InterestedParty interestedParty = new InterestedParty(person, price);
        boolean isPersonAlreadyInterested = isPersonInterested(person);
        if ( !isPersonAlreadyInterested ) {
            this.interestedParties.add(interestedParty);
            repositoryService.persist(interestedParty);
        }
        return this;
    }

    private boolean isPersonInterested(Person person) {
        Iterator<InterestedParty> iter = this.interestedParties.iterator();
        while ( iter.hasNext() ) {
            InterestedParty interestedParty = iter.next();
            if ( person.compareTo(interestedParty.getPerson()) == 0 ) {
                return true;
            }
        }
        return false;
    }

    public List<Person> choices0AddInterestedParty() {
        return getPotentialTenants();
    }

    private List<Person> getPotentialTenants() {
        List<Person> allPersons = persons.listAll();
        List<Person> potentialTenants = new ArrayList<>();
        Iterator<Person> iterator = allPersons.listIterator();
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if ( !this.owners.contains(person) ) {
                potentialTenants.add(person);
            }
        }
        return potentialTenants;
    }

    @Action(
            semantics = SemanticsOf.IDEMPOTENT,
            associateWith = "interestedParties", associateWithSequence = "2"
    )
    public Listing removeInterestedParty(InterestedParty interestedParty) {
        this.interestedParties.remove(interestedParty);
        return this;
    }

    @Collection()
    @Getter @Setter
    private SortedSet<InterestedParty> interestedParties = new TreeSet<InterestedParty>();

    @Action(
            semantics = SemanticsOf.IDEMPOTENT,
            associateWith = "tenants"
    )
    public Listing addTenant(final Person person) {
        if ( !this.tenants.contains(person) ) {
            this.tenants.add(person);
            this.setStatus(Status.OCCUPIED);
            repositoryService.persist(person);
        }
        return this;
    }

    public List<Person> choices0AddTenant() {
        return getPotentialTenants();
    }

    @Action(
            semantics = SemanticsOf.IDEMPOTENT,
            associateWith = "tenants", associateWithSequence = "2"
    )
    public Listing removeTenant(Person person) {
        this.tenants.remove(person);
        if ( this.tenants.isEmpty() ) {
            this.setStatus(Status.VACANT);
        }
        return this;
    }

    @Collection()
    @Getter @Setter
    private SortedSet<Person> tenants = new TreeSet<Person>();

    @Override
    public String toString() {
        return getAddress();
    }

    @Override
    public int compareTo(final Listing other) {
        return Comparator.comparing(Listing::getAddress).compare(this, other);
    }

    @Inject RepositoryService repositoryService;
    @Inject TitleService titleService;
    @Inject MessageService messageService;
    @Inject Persons persons;
}